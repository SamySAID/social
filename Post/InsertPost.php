<?php
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class InsertPost{

	private $link;
	private $output = array();
	private $id, $contenu,$token;
	
	public function __construct(){
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id = $_POST["id"];
		$this->contenu = $_POST["contenu"];
		$this->token = $_POST["token"];//token de vérification
				
		$select = $link->prepare('SELECT * FROM user where idUser = :id');
        $select->execute(array(':id' => $this->id));
		$row = $select->fetch(PDO::FETCH_ASSOC);

		if(empty($this->id) || empty($this->contenu) || empty($this->token)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = null;
                }
		else if($this->token != $row["token"]){
				$output["code"] = 3; //Token invalide
                $output["result"] = null;
			}
		else{
			try{
				//Si tout est OK on insere le post dans la BDD
				$select = $link->prepare('INSERT INTO post (contenu,idUser,date) VALUES (:contenu, :id ,:date)');
        		$select->execute(array(':contenu' => $this->contenu, ':id' => $this->id, ':date' => date('Y-m-d H:i:s') ));
                $output["code"] = 0;
				$output["result"] = array(
					'contenu' => $this->contenu, 
					'id' => $this->id, 
					'date' => date('Y-m-d H:i:s'));
				}
			catch(PDOException $e){
				$output["code"] = 1; //Erreur interne au serveur SQL
                $output["result"] = null;
				}
			}
		echo json_encode($output);
	}

}

new InsertPost();
?>