<?php
header('Content-type: application/json');

require_once('../BaseLink.php');

class ReadPost{
	
	private $link;
	private $output = array();
	private $id_post, $contenu,$token;
	
	public function __construct()
	{
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id_post = $_POST["id_post"];
		
		if(empty($this->id_post) ){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = null;
                }
		else{
				//Si tout est OK on recupere le  post demandé
				$select = $link->prepare('SELECT * FROM post WHERE id_post=:id_post');
                $output["code"] = 0;
				$output["result"] = array(
					':contenu' => $this->contenu, 
					':id_post' => $this->id_post, 
					':date' => date('Y-m-d H:i:s'));
				}
		echo json_encode($output);
	}

}
new ReadPost();
?>