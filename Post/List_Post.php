<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class List_post{

	private $link;
	private $output = array();
	private $id,$limit,$offset;
	
	public function __construct(){
		$this->link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id = $_POST["id"];
        $this->limit = $_POST["limit"];
        $this->offset = $_POST["offset"];
		
		if(empty($this->id)){
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = 1;
                }
		else{
			try{
				$query = $this->link->prepare('SELECT * FROM post where idUser = :id limit :limit offset :offset');
        		$query->execute(array(':id' => $this->id,':limit' => $this->limit,':offset' => $this->offset,));
				$row = $query->fetchAll();
				echo "row : ".$row;
                $output["code"] = 0;
				$output["result"] = array('id' => $row['idPost'], 'contenu' => $row['contenu']);
				//Génération d'un token unique	
            	$_SESSION['token'] = uniqid('', true);
					}
			catch(PDOException $e){
				$output["code"] = 1; //Erreur interne au serveur SQL
                $output["result"] = null;
					}
				}
		echo json_encode($output);
	}

}

new List_post();
?>