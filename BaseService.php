<?php 
ini_set("display_errors", 1);
require_once('BaseLink.php');

abstract class BaseService
{
	private $link;
	private $output = array();

	protected function init()
	{
		$this->link = BaseLink::getSharedBaseLink()->getLink();
		$this->set_params();
		
		if(!$this->are_params_set())
		{
			$this->set_output(5, $this->not_set_message());
		}
		else if($this->test_if_already_done())
		{
			 $this->set_output(6, $this->already_done_message());
		}
		else 
		{
			$this->do_service_job();
		}
		
		$this->json_output();
	}

	protected function get_link()
	{
		return $this->link;
	}

	protected function json_output()
	{
		header('Content-type: application/json');
        echo(json_encode($this->output));
	}

	protected function set_output($code, $result)
	{
		$this->output["code"] = $code;
		$this->output["result"] = $result;
	} 

	private function not_set_message()
	{
		return "Some parameters are not set";
	}

	protected function test_token($id, $token)
	{
	
		try
		{
			$test = $this->link->prepare("SELECT COUNT(*) as nb FROM user WHERE idUser = :id AND token = :token");
			$test->execute(array(':id' => $id, ':token' => $token));
			$row = $test->fetch(PDO::FETCH_ASSOC);

			if(!$row)
				throw new PDOException("Error Processing Request", 1);	
		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while inserting into the database");
			$this->json_output();
			die();
		}

		$count = "";

		foreach ($row as $valt) {
			$count = $valt['nb'];
		}

		if($count == 0)
		{
			$this->set_output(3, "Token error");
			$this->json_output();
			die();
		}	
		else
			return true;
	}


	abstract protected function already_done_message();

	abstract protected function set_params();
	abstract protected function do_service_job();
	abstract protected function test_if_already_done();
	abstract protected function are_params_set();

}

?>