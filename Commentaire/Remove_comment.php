<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Remove_comment{

	private $link;
	private $output = array();
	private $id, $id_comment,$token;
	
	public function __construct(){
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id = $_POST["id"];
        $this->id_comment = $_POST["id_comment"];
		$this->token = $_POST["token"];//token de vérification
		
		$select = $link->prepare('SELECT * FROM user where idUser = :id');
        $select->execute(array(':id' => $this->id));
		$row = $select->fetch(PDO::FETCH_ASSOC);
		
		if(empty($this->id) || empty($this->id_comment) || empty($this->token)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = 1;
                }
		elseif($this->token != $row["token"]){
				$output["code"] = 3; //Token invalide
                $output["result"] = 1;
			}
		else{
            try{
				//Si tout est OK on insere le commentaires dans la BDD
				$select = $link->prepare('DELETE * FROM comment WHERE id_comment = :id_comment');
        		$select->bindParam(':id_comment', $this->id_comment, PDO::PARAM_INT);   
                $select->execute();
                $output["code"] = 0;
				$output["result"] = 0;
                }
            catch(PDOException $e){
                $output["code"] = 1; //Erreur interne au serveur
                $output["result"] = 1;
                }
            }
		echo json_encode($output);
	}

}

new Remove_comment();
?>