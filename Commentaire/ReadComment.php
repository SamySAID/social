<?php
header('Content-type: application/json');

require_once('../BaseLink.php');

class ReadComment
{
	private $link;
	private $output = array();
	private $id_comment, $id_post, $contenu,$token;
	
	public function __construct()
	{
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id_comment = $_POST["id_comment"];
		
		if(empty($this->id_comment)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = null;
                }
		else{
				//Si tout est OK on recupere les commentaires d'un post
				$select = $link->prepare('SELECT * FROM commentaire WHERE id_comment = :id_comment');
				$select->execute(array(':id_comment' => $this->id_comment));
				$row = $select->fetch(PDO::FETCH_ASSOC);
				if(isset($row['id_comment'])){
					$output["code"] = 0;
					$output["result"] = array(
					'id_comment' => $this->id_comment,
					'contenu' => $this->contenu, 
					'id_comment' => $this->id_post, 
					'date' => date('Y-m-d H:i:s'));
					}
				else{
					$output["code"] = 0;
					$output["result"] = null;
				}
			}
		echo json_encode($output);
	}

}
new ReadComment();
?>