<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Modify_comment{

	private $link;
	private $output = array();
	private $id,$id_comment,$contenu,$token;
	
	public function __construct(){
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id = $_POST["id"];
        $this->id_comment = $_POST["id_comment"];
        $this->contenu = $_POST["contenu"];
		$this->token = $_POST["token"];//token de vérification
		
		$select = $link->prepare('SELECT * FROM user where idUser = :id');
        $select->execute(array(':id' => $this->id));
		$row = $select->fetch(PDO::FETCH_ASSOC);
		
		if(empty($this->id) || empty($this->contenu) || empty($this->id_comment) || empty($this->token)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = null;
                }
		elseif($this->token != $row["token"]){
				$output["code"] = 3; //Token invalide
                $output["result"] = null;
			}
		else{
				//Si tout est OK on insere le commentaires dans la BDD
				$select = $link->prepare('UPDATE commentaire SET contenu = :contenu WHERE id_comment = :id_comment');
                $select->bindParam(':contenu', $this->contenu, PDO::PARAM_STR);       
                $select->bindParam(':id_comment', $this->id_comment, PDO::PARAM_STR);
        		$select->execute();
                $output["code"] = 0;
				$output["result"] = array(
					'id_comment' => $this->id_comment,
					'contenu' => $this->contenu, 
					'id_comment' => $this->id_post, 
					'date' => date('Y-m-d H:i:s'));
				}
		echo json_encode($output);
	}

}

new Modify_comment();
?>