<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Login{

	private $link;
	private $output = array();
	private $email, $password,$token;
	
	public function __construct(){
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->email = $_POST["email"];
        $this->password = sha1($_POST["password"]);
		$this->token = uniqid('', true);

		
		$select = $link->prepare('SELECT * FROM user WHERE email = :email and password = :password');
        $select->execute(array(':email' => $this->email, ':password' => $this->password));
		$row = $select->fetch(PDO::FETCH_ASSOC);
		
		 if(empty($this->email) || empty($this->password)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = null;
                }
        elseif(empty($row)){
                $output["code"] = 2; //Login ou mot de passe invalide
                $output["result"] = null;
                }
		else{
				//Génération d'un token unique à chaque login		
				$_SESSION['token'] = uniqid('', true);
                $output["code"] = 0;
				$output["result"] = array(
					'firstname'=>$row['firstname'],
					'lastname'=>$row['lastname'],
					'email'=>$this->email,
					'password'=>$this->password,
					'profil'=>$row['profil'],
					'token' => $this->token
				);
				}
				
		echo json_encode($output);
	}

}

new Login();
?>