<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Modify_user{

	private $link;
	private $output = array();
	private $firstname, $lastname, $email, $password, $new_password, $new_profil;

	public function __construct(){
		$this->link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->email = $_POST["email"];
        $this->password = sha1($_POST["password"]);
		$this->token = $_POST["token"];//token de vérification

		$select = $this->link->prepare('SELECT * FROM user WHERE email = :email and password = :password');
        $select->execute(array(':email' => $this->email, ':password' => $this->password));
		$row = $select->fetch(PDO::FETCH_ASSOC);
		
		 if(empty($this->email) || empty($this->password)){//On vérifie si tous les parametres sont initialisé
                $this->output["code"] = 5; //Paramètre manquant
                $this->output["result"] = null;
                }
        elseif(empty($row)){
                $this->output["code"] = 2; //Login ou mot de passe invalide
                $this->output["result"] = null;
                }
		elseif($this->token != $row["token"]){
				$output["code"] = 3; //Token invalide
                $output["result"] = null;
			}
		else{
                $this->output["code"] = 0;
				}
	}
	
	public function update(){
        if($this->output["code"] == 0){
                if(isset($_POST['new_profil'])){
                        $sql = "UPDATE user SET profil = :new_profil";
                        $stmt = $this->link->prepare($sql);
                        $stmt->bindParam(':new_profil', $_POST['new_profil'], PDO::PARAM_STR);
                        $stmt->execute();
                        }
                if(isset($_POST['new_password'])){
                        $sql = "UPDATE user SET password = :new_password";
                        $stmt = $this->link->prepare($sql);
                        $stmt->bindParam(':new_password', $_POST['new_password'], PDO::PARAM_STR);
                        $stmt->execute();
                        }
                if(isset($_POST['firstname'])){
                        $sql = "UPDATE user SET firstname = :firstname";
                        $stmt = $this->link->prepare($sql);
                        $stmt->bindParam(':firstname', $_POST['firstname'], PDO::PARAM_STR);
                        $stmt->execute();
                        }
                if(isset($_POST['lastname'])){
                        $sql = "UPDATE user SET lastname = :lastname";
                        $stmt = $this->link->prepare($sql);
                        $stmt->bindParam(':lastname', $_POST['lastname'], PDO::PARAM_STR);
                        $stmt->execute();
                        }
        	}
        echo json_encode($this->output);
        }


}

$update = new Modify_user();
$update->update();
?>