<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Subscribe{
	private $link;
	private $output = array();
	private $firstname, $lastname, $email, $password, $profil,$token;
	
	public function __construct(){
	
		$link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		
        //Récupération des variables d'entrées
		$this->email = $_POST["email"];
        $this->firstname = $_POST["firstname"];
		$this->lastname = $_POST["lastname"];
		$this->password = $_POST["password"];
		$this->profil = $_POST["profil"];
		$this->token = uniqid('', true);
		
        //On vérifie si le Nom donnée n'est pas déja présent dans la base de données
        $count = $link->prepare('SELECT * FROM user WHERE email = :email');
        $count->execute(array(':email' => $this->email));
		$row_count = $count->fetch(PDO::FETCH_ASSOC);//$row_count[0] contient le nombre de ligne retourner par le résultat
		echo $row_count['count'];

        if(empty($this->email) || empty($this->firstname) || empty($this->lastname) || empty($this->password) || empty($this->profil)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = null;
                }
        elseif(isset($row_count['email'])){//Si $row_count[0] > 0 ca veut dire qu'il y a deja une entrée dans la base
                $output["code"] = 4; //email déja utilisé
                $output["result"] = null;
                }
        else{
                try{
						//Si tout est ok on insert une nouvelle ligne dans la base de donnée
                        $insert = $link->prepare('INSERT INTO user (firstname,lastname,email,password,profil,token) VALUES (:firstname,:lastname,:email,:password,:profil,:token)');
                        $insert->execute(array(
                                ":firstname" => $this->firstname,
                                ":lastname" => $this->lastname,
                                ":email" => $this->email,
                                ":password" => sha1($this->password),
                                ":profil" => $this->profil,
								":token" => $this->token));
                        $output["code"] = 0;
                        }
                catch(Exception $e){
						//On vérifie s'il n'y a pas eu d'erruer SQL
                        $output["code"] = 1; //Erreur interne au serveur SQL
                        $output["result"] = null;
                        }
                }
		if($output["code"] == 0){
            //Génération d'un token unique	
			$output["result"] = array('firstname'=>$this->firstname,'lastname'=>$this->lastname,'email'=>$this->email,'password'=>$this->password,'profil'=>$this->profil,'token' => $this->token);
			}
            
        echo json_encode($output);

	}

}

new Subscribe();
?>