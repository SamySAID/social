<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Remove_user{

	private $link;
	private $output = array();
	private $email, $password;

	public function __construct(){
		$this->link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->email = $_POST["email"];
        $this->password = sha1($_POST["password"]);
		$this->token = $_POST["token"];//token de vérification


		$select = $this->link->prepare('SELECT * FROM user WHERE email = :email and password = :password');
        $select->execute(array(':email' => $this->email, ':password' => $this->password));
		$row = $select->fetch(PDO::FETCH_ASSOC);		
		
		$select = $this->link->prepare('SELECT * FROM user where idUser = :id');
        $select->execute(array(':id' => $this->id));
		$row_tok = $select->fetch(PDO::FETCH_ASSOC);
		
		 if(empty($this->email) || empty($this->password)){//On vérifie si tous les parametres sont initialisé
                $this->output["code"] = 5; //Paramètre manquant
                $this->output["result"] = 1;
                }
        elseif(empty($row)){
                $this->output["code"] = 2; //Login ou mot de passe invalide
                $this->output["result"] = 1;
                }
		elseif($this->token != $row["token"]){
				$output["code"] = 3; //Token invalide
                $output["result"] = null;
			}
		else{
				$sql = "DELETE FROM user WHERE email =  :email";
				$stmt = $this->link->prepare($sql);
				$stmt->bindParam(':email', $this->email, PDO::PARAM_INT);   
				$stmt->execute();
                $this->output["code"] = 0;
				$this->output["result"] = 0;
				}				
		echo json_encode($this->output);
	}
	
}

new Remove_user();

?>