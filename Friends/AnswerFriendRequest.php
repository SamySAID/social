<?php 

require_once('../BaseService.php');

class AnswerFriendRequest extends BaseService
{

	private $target_id;
	private $my_id;
	private $token;
	private $answer;

	public function __construct(){

		parent::init();
	}

	protected function set_params()
	{
		$this->target_id = $_POST["target_id"];
		$this->my_id = $_POST["my_id"];
		$this->token = $_POST["token"];
		$this->answer = $_POST["answer"];
	}

	protected function are_params_set()
	{	
		if(!isset($this->target_id) || !isset($this->my_id) || !isset($this->token) || !isset($this->answer) )
			return false;
		else 
		{
			parent::test_token($this->my_id, $this->token);
			return true;
		}
			
	}

	protected function already_done_message()
	{	
		//because already done do not correspond, we die and show the right error;
		parent::set_output(8,"No corresponding pending request found");
		parent::json_output();
		die();
	}

	protected function test_if_already_done() 
	{

		try
		{
			//Here it's not already done, but : this request do not exist
			//We test if the user made a request for the asker (us)
			$link = parent::get_link();
			$select = $link->prepare("SELECT COUNT(*) as nb FROM friend_request WHERE idUser = :asker AND idUser_asker = :id ");
			$test = $select->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if(!$test)
				throw new PDOException("Error Processing Request", 1);	

			$row = $select->fetch(PDO::FETCH_ASSOC);
			$id = 1;
			$id = $row['nb'];

			if($id > 0)
				return false;
			else
				return true;

		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}	
		
	}
	
	protected function do_service_job()
	{
		
		try{

			//delete the request the other user made
			$link = parent::get_link();
			$delete = $link->prepare("DELETE FROM friend_request WHERE (idUser = :asker AND idUser_asker = :id)");
			$delete->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if($this->answer =="true")
			{
				$insert = $link->prepare("INSERT INTO relation (idUser, idFriend) VALUES (:id, :asker)");
				$test = $insert->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

				if(!$test)
					throw new PDOException("Error Processing Request", 1);	

				$insert = $link->prepare("INSERT INTO relation (idUser, idFriend) VALUES (:asker, :id)");
				$test = $insert->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

				if(!$test)
					throw new PDOException("Error Processing Request", 1);

				parent::set_output(9,"Friend request accepted with success");
			}
			else if ($this->answer == "false") 
			{
				parent::set_output(10,"Friend request refused with success");
			}
			else 
			{
				parent::set_output(5, $this->not_set_message());
				parent::json_output();
				die();
			}

		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while inserting into the database");

		}
	}
		
}

$test = new AnswerFriendRequest();

?>