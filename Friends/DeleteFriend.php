<?php 

require_once('../BaseService.php');

class DeleteFriend extends BaseService
{

	private $target_id;
	private $my_id;
	private $token;

	public function __construct(){

		parent::init();
	}

	protected function already_done_message()
	{
		return "You already have removed this user / or he is not in your friend list";
	}

	protected function set_params()
	{
		$this->target_id = $_POST["target_id"];
		$this->my_id = $_POST["my_id"];
		$this->token = $_POST["token"];
	}

	protected function are_params_set()
	{	
		if(!isset($this->target_id) || !isset($this->my_id) || !isset($this->token) )
			return false;
		else 
		{
			parent::test_token($this->my_id, $this->token);
			return true;
		}
			
	}

	protected function test_if_already_done() 
	{

		try
		{
			$link = parent::get_link();
			$select = $link->prepare("SELECT COUNT(*) as nb FROM relation WHERE (idUser = :id AND idFriend = :asker) OR 
				(idUser = :asker AND idFriend = :id) ");
			
			$test = $select->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));
			
			if(!$test)
			{
				throw new PDOException("Error Processing Request", 1);	
			}

			$row = $select->fetch(PDO::FETCH_ASSOC);
			$id = 1;
			$id = $row['nb'];
			
			//if there is already a friend relation 
			if($id > 0)
				return false;
			else
				return true;

		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
		
		
	}
	
	protected function do_service_job()
	{
		
		try{

			$link = parent::get_link();
			$delete = $link->prepare("DELETE FROM relation WHERE ( idUser = :id AND idFriend = :asker) OR 
				( idUser = :asker AND idFriend = :id)");
			$test = $delete->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if($test)
			{
				parent::set_output(0,"Friend deleted with success");
			}
			else 
			{
				throw new PDOException("Error Processing Request", 1);	
			}
			
		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
	}
			
}

$test = new DeleteFriend();

?>