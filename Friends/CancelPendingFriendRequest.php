<?php 

require_once('../BaseService.php');

class CancelPendingFriendRequest extends BaseService
{

	private $target_id;
	private $my_id;
	private $token;

	public function __construct(){

		parent::init();
	}
	
	protected function already_done_message()
	{
		return "You already have removed this request / no request matching";
	}

	protected function set_params()
	{
		$this->target_id = $_POST["target_id"];
		$this->my_id = $_POST["my_id"];
		$this->token = $_POST["token"];
	}
	
	protected function are_params_set()
	{	
		if(!isset($this->target_id) || !isset($this->my_id) || !isset($this->token) )
			return false;
		else 
		{
			parent::test_token($this->my_id, $this->token);
			return true;
		}
			
	}

	protected function test_if_already_done() 
	{

		try
		{
			//Checking if we made this request
			$link = parent::get_link();
			$select = $link->prepare("SELECT COUNT(*) as nb FROM friend_request WHERE idUser = :id AND idUser_asker = :asker ");
			$test = $select->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));
			
			if(!$test)
			{
				throw new PDOException("Error Processing Request", 1);	
			}

			$row = $select->fetch(PDO::FETCH_ASSOC);
			$id = 0;
			$id = $row['nb'];
			
			//if there is no friend relation
			if($id > 0)
				return false;
			else
				return true;

		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
		
		
	}
	
	protected function do_service_job()
	{
		
		try{

			//delete the request we made
			$link = parent::get_link();
			$delete = $link->prepare("DELETE FROM friend_request WHERE (idUser = :id AND idUser_asker = :asker)");
			$test = $delete->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if($test)
			{
				parent::set_output(0,"Friend request deleted with success");
			}
			else 
			{
				throw new PDOException("Error Processing Request", 1);	
			}
			
		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
	}
			
}

$test = new CancelPendingFriendRequest();

?>