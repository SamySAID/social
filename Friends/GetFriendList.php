<?php 

require_once('../BaseService.php');

class GetFriendList extends BaseService
{	

	private $offset;
	private $limit;
	private $my_id;
	private $token;

	public function __construct(){

		parent::init();
	}

	
	protected function already_done_message()
	{
		return null; //not applicable
	}

	protected function set_params()
	{	

		$this->offset = $_POST["offset"];
		$this->limit =  $_POST["limit"];
		$this->my_id = $_POST["my_id"];
		$this->token = $_POST["token"];
	}
	
	protected function are_params_set()
	{	
		//Check for this
		if(!isset($this->limit) || !isset($this->offset) || !isset($this->my_id) || !isset($this->token)  )
			return false;
		else 
		{
			parent::test_token($this->my_id, $this->token);
			return true;
		}
			
	}

	protected function test_if_already_done() 
	{
		//not applicable
		return false;		
	}
	
	protected function do_service_job()
	{
	
		try{

			$limitBis = (int) $this->limit;
			$offsetBis = (int) $this->offset;

			$link = parent::get_link();
			$select = $link->prepare("SELECT * FROM User WHERE idUser IN ( SELECT idFriend from Relation where idUser = :asker) 
				LIMIT :offset, :limited");

			$select->bindParam(':limited', $limitBis , PDO::PARAM_INT);
			$select->bindParam(':offset',  $offsetBis , PDO::PARAM_INT);
			$select->bindParam(':asker', $this->my_id , PDO::PARAM_INT);
			$test = $select->execute();
			

			$out = $select->fetchAll(PDO::FETCH_ASSOC);
	
			if($test)
			{	
				parent::set_output(0, $out);
			}
			else 
			{
				throw new PDOException("Error Processing Request", 1);	
			}
			
		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
	}
			
}

$test = new GetFriendList();

?>