<?php 

require_once('../BaseService.php');



class UsersWithMostPosts extends BaseService
{	
	
	private $offset;
	private $limit;

	public function __construct(){

		parent::init();
	}

	
	protected function already_done_message()
	{
		return null; //not applicable
	}

	protected function set_params()
	{	
		$this->offset = $_POST["offset"];
		$this->limit =  $_POST["limit"];
		$this->my_id = $_POST["my_id"];
		$this->token = $_POST["token"];
		$this->request = $_POST["request"];
	}
	
	protected function are_params_set()
	{	
	
		if(!isset($this->limit) || !isset($this->offset))
			return false;
		else 
		{
			return true;
		}
			
	}

	protected function test_if_already_done() 
	{
		//not applicable
		return false;		
	}
	
	protected function do_service_job()
	{
	
		try{

			//manque savoir si c'est un amis
			$limitBis = (int) $this->limit;
			$offsetBis = (int) $this->offset;

			$link = parent::get_link();
			$select = $link->prepare("SELECT COUNT(*) as count_post, User.idUser, User.firstname, 
				User.lastname, User.profil, User.password, User.email, User.token 
				FROM USER 
				INNER JOIN Post ON user.idUser = Post.idUser 
				GROUP BY user.idUser 
				ORDER BY count_post DESC
				LIMIT :offset, :limited");

			$select->bindParam(':limited', $limitBis , PDO::PARAM_INT);
			$select->bindParam(':offset',  $offsetBis , PDO::PARAM_INT);

			$test = $select->execute();

			if(!$test)
			{
				throw new PDOException("Error Processing Request", 1);
			}
		
			$out = $select->fetchAll(PDO::FETCH_ASSOC);
			
			if($test)
			{	
				parent::set_output(0, $out);
			}
			else 
			{
				throw new PDOException("Error Processing Request", 1);	
			}
			
		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
	}
			
}

$test = new UsersWithMostPosts();

?>