<?php 

require_once('../BaseService.php');

class FriendRequest extends BaseService
{

	private $target_id;
	private $my_id;
	private $token;

	public function __construct(){

		parent::init();
	}

	protected function already_done_message()
	{
		return "You already have a friend request for this user";
	}

	protected function set_params()
	{
		$this->target_id = $_POST["target_id"];
		$this->my_id = $_POST["my_id"];
		$this->token = $_POST["token"];
	}

	protected function are_params_set()
	{	
		if(!isset($this->target_id) || !isset($this->my_id) || !isset($this->token) )
			return false;
		else 
		{
			if($this->target_id == $this->my_id)
			{
				parent::set_output(7,"You cannot friend yourself");
				parent::json_output();
				die();
			}

			parent::test_token($this->my_id, $this->token);
			return true;
		}
			
	}
			
	protected function test_if_already_done() 
	{

		try{

			//We are getting the friend request for you or him 
			//If there is already one, from you or him, you cannot add another
			$link = parent::get_link();
			$select = $link->prepare("SELECT COUNT(*) as nb FROM friend_request WHERE (idUser = :id AND idUser_asker = :asker) 
				OR (idUser = :asker AND idUser_asker = :id)");
			$test = $select->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if(!$test)
				throw new PDOException("Error Processing Request", 1);	
			
			$row = $select->fetch(PDO::FETCH_ASSOC);
			$id = 0;
			$id = $row['nb'];
			
			//Here we test if the users are not already friends
			$select = $link->prepare("SELECT COUNT(*) as nb FROM relation WHERE (idUser = :id AND idFriend = :asker) 
				OR (idUser = :asker AND idFriend = :id)");
			$test = $select->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if(!$test)
				throw new PDOException("Error Processing Request", 1);	

			$row = $select->fetch(PDO::FETCH_ASSOC);
			$idBis = 0;
			$idBis = $row['nb'];
			
			if($idBis > 0)
			{
				parent::set_output(11,"This user is already your friend");
				parent::json_output();
				die();
			}

			if($id == 0)
				return false;
			else
				return true;

		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");
		}
		
		
	}

	protected function do_service_job()
	{
		
		try{

			$link = parent::get_link();
			$insert = $link->prepare("INSERT INTO friend_request (idUser, idUser_asker) VALUES (:id, :asker)");
			$test = $insert->execute(array(':id' => $this->target_id, ':asker' => $this->my_id));

			if(!$test)
				throw new PDOException("Error Processing Request", 1);	

			parent::set_output(0,"Friend request created with success");
		}
		catch(PDOException $e)
		{
			parent::set_output(1,"Error while working with the database");

		}
	}
}

$test = new FriendRequest();

?>