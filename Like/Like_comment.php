<?php
header('Content-type: application/json');
//ini_set('display_error',1);//Pour le debug seulement
require_once('../BaseLink.php');

class Like_post{

	private $link;
	private $output = array();
	private $id, $id_comment,$token;
	
	public function __construct(){
		$this->link = BaseLink::getSharedBaseLink()->getLink();//On récupere la connexion pdo depuis la classe BaseLink
		$this->id = $_POST["id"];
        $this->id_comment = $_POST["id_comment"];
		$this->token = $_POST["token"];//token de vérification
		
		$test = $this->link->prepare("SELECT COUNT(*) as nb FROM user WHERE idUser = :id AND token = :token");
		$test->execute(array(':id' => $this->id, ':token' => $this->token));
		$row = $test->fetch(PDO::FETCH_ASSOC);
		
		$count = "";

		foreach ($row as $valt) {
			$count = $valt['nb'];
		}

		if(empty($this->id) || empty($this->id_comment) || empty($this->token)){//On vérifie si tous les parametres sont initialisé
                $output["code"] = 5; //Paramètre manquant
                $output["result"] = 1;
                }
		elseif($count == 0){
				$output["code"] = 3; //Token invalide
                $output["result"] = 1;
				}
		else{
			try{
				$like_query = $this->link->prepare('INSERT INTO like (User_idUser, idCommentaire) VALUES (:id, :id_comment)');
        		$like_query->execute(array(':id_user' => $this->id_user, ':id_comment' => $this->id_comment));
                $output["code"] = 0;
				$output["result"] = 0;
				//Génération d'un token unique	
            	$_SESSION['token'] = uniqid('', true);
					}
			catch(PDOException $e){
				$output["code"] = 1; //Erreur interne au serveur SQL
                $output["result"] = null;
					}
				}
		echo json_encode($output);
	}

}

new Like_post();
?>